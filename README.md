# dcc-sig

Digital signature for the DCC.

## References

### General information on XML signature

* [XML Signature Syntax and Processing](https://www.w3.org/TR/xmldsig-core1/)
* [Digitale Signatur bei XML-Dokumenten (XML-Signatur)](http://www.globaltrust.eu/php/cms_monitor.php?q=PUB&s=18457ppt) 

### General information on Canonicalization of XML (C14N)

* [Canonical XML](https://www.w3.org/TR/2001/REC-xml-c14n-20010315)
* [Exclusive XML Canonicalization](https://www.w3.org/TR/2002/REC-xml-exc-c14n-20020718/)

### XML signature software

* [SignXML: Python XML Signature library](https://pypi.org/project/signxml/)
* [DSS Cookbook (eSignature Documentation)](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/DSS+Cookbook)
* ...

* Aalto Smartcom
  * [DCC-API](https://gitlab.com/aalto-smartcom/dcc-api) 
  * [dss-demonstrations](https://github.com/AaltoSmartCom/dss-demonstrations)
  * [Documentation on using the DSS REST-api](https://github.com/AaltoSmartCom/dss-demonstrations/tree/master/documentation)

### Canonical XML software

* [XML Canonicalization (C14N) in Python using lxml](https://www.decalage.info/en/python/lxml-c14n)
* [Canonical XML Version 2.0 pure python implementation](https://github.com/dept2/c14n2py)
